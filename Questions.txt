Jennifer Brown
jlb315@lehigh.edu

1. Using mlab to host a Mongo database is desirable because it obviates the need for you to purchase and maintain your own infrastructure. With mlab, you can scale up or down as needed without needing to update your equipment. However, with a hosting service like mlab you have no control over how the data is stored or the way your machine is implemented. You are also at mlab's mercy in the event of a power outage or other event that affects their servers.

2. 

3. This app executes the second factor, explicitly declare and isolate dependencies, very well. All the dependencies for the app are listed in the top-level package.json file and uses node's package manager npm. This makes deploying the app for developers very easy since they will only have to have node installed before they download the source code. They can then download all the app's dependencies using the command 'npm install'. 

4. This app doesn't follow the third factor, store config in the environment. Instead of storing configuration variables in the environment, the app uses several config files to store credentials for its services like Mongo or OAuth. Even though all the config files are saved in a central location in the config/ folder, this still runs the risk of having the config files accidentally committed to a repository. 

5. 


